![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

* A kind of safeguard, lighthouse in the sea of bits of procedures and good practices to elicit our Bitbucket and Github's repositories. This is a vice-versa road where there is no left and rights, but there is ~~no~~ synergy between both
* This repo is a living document that will grow and adapt over time

### What is this repository for? ###

* Quick summary
    - Bitbucket to Github checklist, _mostly_ for internal use
    
	  ![gif_animated.gif](https://i.ibb.co/RQNwd73/ezgif-2-2e162bd769b9.gif)
    
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - Check [`checklist.md`](https://bitbucket.org/imhicihu/github-transfers/src/master/checklist.md)
* Configuration
    - Needs that both Bitbucket and Github accounts (previously created) and authenticated by both service providers. Check our [previous checklist](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/src/8c4fd4610fe0489b1fb642561a0e8e8ffb9ff863/Github_desktop_app_installation.md) to install Github app under a proxy-umbrella
* Dependencies
    - No dependencies declared
* Database configuration
    - There is no database involved
* Deployment instructions
    - Verify the steps to be done from our [`checklist.md`](https://bitbucket.org/imhicihu/github-transfers/src/master/checklist.md)
    - Install the [native app](https://desktop.github.com/) (according your operating system)
    - Once installed, follow this [guidelines](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/src/master/Github_desktop_app_installation.md) to be proefficient inside our working environment, _aka_ under our proxy server

### Related repositories ###

* Some repositories linked with this project:
     - [Setting up github under proxy](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/)
     - [Good practices on repository creation](https://bitbucket.org/imhicihu/good-practices-on-repository-creation/src/)

### Source ###

* Check them on [here](https://bitbucket.org/imhicihu/github-transfers/src)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/github-transfers/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/github-transfers/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/github-transfers/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/github-transfers/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### License ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png) 